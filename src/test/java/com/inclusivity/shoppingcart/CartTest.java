package com.inclusivity.shoppingcart;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.MessageFormat;

import static java.text.MessageFormat.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CartTest {
    private static final String DOVE_SOAP_PRODUCT_NAME = "Dove Soap";
    private static final String AXE_DEO_PRODUCT_NAME = "Axe Deo";
    private static final BigDecimal DOVE_SOAP_UNIT_PRICE = BigDecimal.valueOf(39.99);
    private static final BigDecimal AXE_DEO_UNIT_PRICE = BigDecimal.valueOf(99.99);
    private static final BigDecimal TAX_RATE = BigDecimal.valueOf(12.5);
    private static final int COMPARISON_RESULT_EQUAL = 0;
    private Cart cart;

    @Before
    public void setUp() {
        cart = new Cart();
    }

    @Test
    public void shouldCalculateTheTotalPriceOfTheCartWhenFiveDoveSoapProductsAreAddedToTheCart() {
        Product doveSoap = new Product(DOVE_SOAP_PRODUCT_NAME, new Price(DOVE_SOAP_UNIT_PRICE));

        BigDecimal noOfProductToAddToTheCart = BigDecimal.valueOf(5D);
        this.cart.addProduct(doveSoap, noOfProductToAddToTheCart);

        CartItem doveSoapCartItem = this.cart.getCartItemFor(doveSoap);
        assertThat(doveSoapCartItem.getQuantity().compareTo(noOfProductToAddToTheCart), equalTo(COMPARISON_RESULT_EQUAL));

        BigDecimal unitPrice = doveSoapCartItem.getProduct().getPrice().getUnitPrice();
        assertThat(unitPrice.compareTo(DOVE_SOAP_UNIT_PRICE), equalTo(COMPARISON_RESULT_EQUAL));

        BigDecimal totalPrice = this.cart.getTotalPrice();
        assertThat(totalPrice.compareTo(new BigDecimal("199.95")), equalTo(COMPARISON_RESULT_EQUAL));
    }


    @Test
    public void shouldCalculateTheTotalPriceOfTheCartWhenFiveAndThenThreeDoveProductsAreAddedToTheCart() {
        Product doveSoap = new Product(DOVE_SOAP_PRODUCT_NAME, new Price(DOVE_SOAP_UNIT_PRICE));
        this.cart.addProduct(doveSoap, BigDecimal.valueOf(5));
        this.cart.addProduct(doveSoap, BigDecimal.valueOf(3));

        CartItem doveSoapCartItem = this.cart.getCartItemFor(doveSoap);
        assertThat(doveSoapCartItem.getQuantity().compareTo(BigDecimal.valueOf(8)), equalTo(COMPARISON_RESULT_EQUAL));

        BigDecimal doveSoapUnitPrice = doveSoapCartItem.getProduct().getPrice().getUnitPrice();
        assertThat(doveSoapUnitPrice.compareTo(DOVE_SOAP_UNIT_PRICE), equalTo(COMPARISON_RESULT_EQUAL));

        BigDecimal doveSoapTotalPrice = this.cart.getTotalPrice();
        assertThat(doveSoapTotalPrice.compareTo(new BigDecimal("319.92")), equalTo(COMPARISON_RESULT_EQUAL));
    }

    @Test
    public void shouldCalculateTotalPriceAndTaxRateOfShoppingCartWhenMultipleItemsAreAddedToTheCart() {
        Product doveSoap = new Product(DOVE_SOAP_PRODUCT_NAME, new Price(DOVE_SOAP_UNIT_PRICE, TAX_RATE));
        Product axeDeo = new Product(AXE_DEO_PRODUCT_NAME, new Price(AXE_DEO_UNIT_PRICE, TAX_RATE));

        this.cart.addProduct(doveSoap, BigDecimal.valueOf(2));
        this.cart.addProduct(axeDeo, BigDecimal.valueOf(2));

        CartItem doveSoapCartItem = this.cart.getCartItemFor(doveSoap);
        assertThat(
                "Expected shopping cart to contain 2 Dove Soaps",
                doveSoapCartItem.getQuantity().compareTo(BigDecimal.valueOf(2)),
                equalTo(COMPARISON_RESULT_EQUAL)
        );

        assertThat(
                "Expected shopping cart to contain Dove Soaps with unit price of 39.99",
                doveSoapCartItem.getProduct().getPrice().getUnitPrice().compareTo(DOVE_SOAP_UNIT_PRICE),
                equalTo(COMPARISON_RESULT_EQUAL)
        );

        CartItem axeDeoCartItem = this.cart.getCartItemFor(axeDeo);
        assertThat(
                "Expected shopping cart to contain 2 Axe Deos",
                axeDeoCartItem.getQuantity().compareTo(BigDecimal.valueOf(2)),
                equalTo(COMPARISON_RESULT_EQUAL)
        );

        assertThat(
                "Expected shopping cart to contain Axe Does with unit price of 99.99",
                doveSoapCartItem.getProduct().getPrice().getUnitPrice().compareTo(DOVE_SOAP_UNIT_PRICE),
                equalTo(COMPARISON_RESULT_EQUAL)
        );

        assertThat(
                format("Expected shopping cart total tax amount of {0} to be 35.00", this.cart.getTotalTax()),
                this.cart.getTotalTax().compareTo(new BigDecimal("35.00")),
                equalTo(COMPARISON_RESULT_EQUAL)
        );

        assertThat(
                format("Expected shopping cart total price amount of {0} to be 314.96", this.cart.getTotalPrice()),
                this.cart.getTotalPrice().compareTo(new BigDecimal("314.96")),
                equalTo(COMPARISON_RESULT_EQUAL)
        );
    }
}