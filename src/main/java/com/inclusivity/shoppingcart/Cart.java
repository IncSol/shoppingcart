package com.inclusivity.shoppingcart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class Cart {
    public static final int DEFAULT_PRECISION = 2;
    private Map<Product, CartItem> shoppingCartItems;
    private BigDecimal subtotal;
    private BigDecimal totalTax;

    public Cart() {
        shoppingCartItems = new LinkedHashMap<>();
    }

    public void addProduct(Product product, BigDecimal quantity) {
        CartItem cartItem = Optional.ofNullable(shoppingCartItems.get(product))
                .map(item -> new CartItem(item.getQuantity().add(quantity), product))
                .orElseGet(() -> new CartItem(quantity, product));

        this.shoppingCartItems.put(product, cartItem);
        this.subtotal = this.calculateSubTotalPrice();
        this.totalTax = this.calculateTotalTax();
    }

    private BigDecimal calculateTotalTax() {
        return this.shoppingCartItems.values().stream()
                .map(CartItem::getTotalTax)
                .reduce(null, (current, next) -> {
                    if (current == null) return next;
                    return current.add(next);
                });
    }

    private BigDecimal calculateSubTotalPrice() {
        return this.shoppingCartItems.values().stream()
                .map(CartItem::getTotalPrice)
                .reduce(null, (current, next) -> {
                    if (current == null) return next;
                    return current.add(next);
                });
    }

    public BigDecimal getTotalPrice() {
        return this.subtotal
                .add(this.totalTax)
                .setScale(DEFAULT_PRECISION, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotalTax() {
        return this.totalTax.setScale(DEFAULT_PRECISION, RoundingMode.HALF_UP);
    }

    public CartItem getCartItemFor(Product product) {
        return this.shoppingCartItems.get(product);
    }
}
