package com.inclusivity.shoppingcart;

import java.math.BigDecimal;

public class CartItem {
    private BigDecimal quantity;
    private Product product;

    public CartItem(BigDecimal quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public BigDecimal getTotalPrice() {
        return this.getProduct().getPrice().getUnitPrice().multiply(this.getQuantity());
    }

    public BigDecimal getTotalTax() {
        return getTotalPrice().multiply(getProduct().getPrice().getTaxRate()).divide(BigDecimal.valueOf(100));
    }
}
