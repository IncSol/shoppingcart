package com.inclusivity.shoppingcart;

import java.math.BigDecimal;

public class Price {
    private BigDecimal unitPrice;
    private BigDecimal taxRate;

    public Price(BigDecimal unitPrice) {
        this(unitPrice, BigDecimal.ZERO);
    }

    public Price(BigDecimal unitPrice, BigDecimal taxRate) {
        this.unitPrice = unitPrice;
        this.taxRate = taxRate;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }
}
